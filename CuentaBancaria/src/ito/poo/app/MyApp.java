package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.CuentaBancaria;

public class MyApp {
	
	static void run() {
		CuentaBancaria CB1=new CuentaBancaria(13605,"Armando Banquitos", 50000f,LocalDate.of(2018, 8, 21));
		
		System.out.println(CB1);
		System.out.println(CB1.retiro(98000f));
		System.out.println(CB1);
		System.out.println(CB1.deposito(10000f));
		System.out.println(CB1);
		
		CuentaBancaria CB2=new CuentaBancaria(13605,"Armando Banquitos", 50000f,LocalDate.of(2018, 8, 21));
		
		System.out.println(CB2);
		System.out.println(CB2.retiro(98000f));
		System.out.println(CB2);
		System.out.println(CB2.deposito(10000f));
		System.out.println(CB2);
		
		CuentaBancaria CB3=new CuentaBancaria(32795,"Armando Banquitos", 35800f,LocalDate.of(2020, 7, 05));
		CuentaBancaria CB4=new CuentaBancaria(54321,"Luis Ramirez", 35800f,LocalDate.of(2018, 8, 21));
		
		
		System.out.println();
		if (CB1 == CB2)
			System.out.println("Los objetos son iguales");
		else
			System.out.println("Los objetos no son iguales");
		
		System.out.println(CB1.compareTo(CB2));
		System.out.println(CB1.equals(CB2));
		
		System.out.println();
		
		System.out.println(CB1.compareTo(CB3));
		System.out.println(CB1.equals(CB3));
		
		System.out.println(CB1.compareTo(CB4));
		System.out.println(CB1.equals(CB4));
		
	}
	
	public static void main(String[] args) {
		run();
	}

}
