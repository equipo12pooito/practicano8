package app.ito.poo;
import clases.ito.poo.CuerpoCeleste;
import clases.ito.poo.Ubicacion;

public class MyApp {
	
	static void run() {
		CuerpoCeleste c1 = new CuerpoCeleste("Sol", "Gaseoso");
		c1.addLocalizaciones(new Ubicacion(1363, 8920, "Solsticio de verano", 43256));
		c1.addLocalizaciones(new Ubicacion(6724.22F, 5230.5F, "2 semanas", 325623));
		c1.addLocalizaciones(new Ubicacion(1234, 5678, "6 meses", 235667));
		System.out.println(c1);
		System.out.println(c1.desplazamiento(5000, 4000));
		System.out.println(c1.desplazamiento(1363, 3246));
		System.out.println(c1.desplazamiento(1363, 8920));
		System.out.println(c1.desplazamiento(6724.22F,  5230.5F));
		
		CuerpoCeleste c2 = new CuerpoCeleste("Sol", "Gaseoso");
		CuerpoCeleste c3 = new CuerpoCeleste("Marte", "Rocoso");
		
		System.out.println();
		if (c1 == c2)
			System.out.println("Los objetos son iguales");
		else
			System.out.println("Los objetos no son iguales");
		
		System.out.println();
		System.out.println(c1.compareTo(c2));
		System.out.println(c1.equals(c2));
		System.out.println();
		System.out.println(c1.compareTo(c3));
		System.out.println(c1.equals(c3));
		
		c1.addLocalizaciones(new Ubicacion(1363, 8920, "Solsticio de verano", 43256));
		System.out.println();
		System.out.println(c1.getLocalizacion(0).compareTo(c1.getLocalizacion(3)));
		System.out.println(c1.getLocalizacion(0).equals(c1.getLocalizacion(3)));
		System.out.println();
		System.out.println(c1.getLocalizacion(1).compareTo(c1.getLocalizacion(2)));
		System.out.println(c1.getLocalizacion(1).equals(c1.getLocalizacion(2)));
	}

	public static void main(String[] args) {
		run();
	}

}
