package clases.ito.poo;

import java.util.Comparator;

public class Ubicacion implements Comparable <Ubicacion> {
	
	private float longitud;
	private float latitud;
	private String periodo;
	private float distancia;
	
	public Ubicacion() {
		super();
	}
	
	public Ubicacion(float longitud, float latitud, String periodo, float distancia) {
		super();
		this.longitud = longitud;
		this.latitud = latitud;
		this.periodo = periodo;
		this.distancia = distancia;
	}

	//===============================
	
	public float getLongitud() {
		return longitud;
	}
	
	public void setLongitud(float longitud) {
		this.longitud = longitud;
	}
	
	public float getLatitud() {
		return latitud;
	}
	
	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}
	
	public String getPeriodo() {
		return periodo;
	}
	
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	public float getDistancia() {
		return distancia;
	}
	
	public void setDistancia(float distancia) {
		this.distancia = distancia;
	}
	
	//===============================

	@Override
	public String toString() {
		return "(Longitud: " + longitud + ", Latitud: " + latitud + ", Periodo: " + periodo + ", Distancia: "
				+ distancia + ")";
	}

	@Override
	public int compareTo(Ubicacion o) {
		return Comparator.comparing(Ubicacion::getPeriodo)
				.thenComparingDouble(Ubicacion::getDistancia)
				.thenComparingDouble(Ubicacion::getLongitud)
				.thenComparingDouble(Ubicacion::getLatitud)
				.compare(this, o);
	}

	@Override
	public boolean equals(Object obj) {
		Ubicacion o = (Ubicacion) obj;
		if (periodo == o.getPeriodo())
			if (distancia == o.getDistancia())
				if (longitud == o.getLongitud())
					if (latitud == o.getLatitud())
						return true;
		return false;
	}
	
}
