package clases.ito.poo;
import java.util.ArrayList;
import java.util.Comparator;

public class CuerpoCeleste implements Comparable <CuerpoCeleste> {

	private String nombre;
	private ArrayList<Ubicacion> localizaciones = new ArrayList<Ubicacion>();
	private String composicion;
	
	public CuerpoCeleste() {
		super();
	}
	
	public CuerpoCeleste(String nombre, String composicion) {
		super();
		this.nombre = nombre;
		this.composicion = composicion;
	}

	//===============================
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Ubicacion> getLocalizaciones() {
		return localizaciones;
	}
	
	public Ubicacion getLocalizacion (int i) {
		Ubicacion o = null;
		if (i >= 0  || i <= this.localizaciones.size())
			o = this.localizaciones.get(i);
		return o;
	}

	public void addLocalizaciones(Ubicacion localizaciones) {
		this.localizaciones.add(localizaciones);
	}

	public String getComposicion() {
		return composicion;
	}

	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}

	//===============================
	
	public boolean encontrarUbicacion(float i, float j) {
		boolean aux = false;
		for (int e = 0; e < this.localizaciones.size(); e++) 
			if (this.localizaciones.get(e).getLongitud() == i && 
				this.localizaciones.get(e).getLatitud() == j)
				aux = true;	
		return aux;
	}

	public float desplazamiento(float i, float j) {
		float a = 0;
		if (encontrarUbicacion(i, j) == true)
			a = Math.abs(i - j);
		else
			a = -1;
		return a;
	}
	
	//===============================

	@Override
	public String toString() {
		return "Cuerpo Celeste: " + nombre + "\nLocalizaciones: " + localizaciones + "\nComposicion: " + composicion;
	}

	@Override
	public int compareTo(CuerpoCeleste o) {
		return Comparator.comparing(CuerpoCeleste::getNombre)
				.thenComparing(CuerpoCeleste::getComposicion)
				.compare(this, o);
	}

	@Override
	public boolean equals(Object obj) {
		CuerpoCeleste o = (CuerpoCeleste) obj;
		if (nombre == o.getNombre())
			if (composicion == o.getComposicion())
				return true;
		return false;
	}
	
	

}
