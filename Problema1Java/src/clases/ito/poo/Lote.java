package clases.ito.poo;
import java.time.LocalDate;
import java.util.Comparator;

public class Lote implements Comparable<Lote> {

	private int numLote;
	private int numPiezas;
	private LocalDate fecha;
	
	public Lote() {
		super();
	}
	
	public Lote(int numLote, int numPiezas, LocalDate fecha) {
		super();
		this.numLote = numLote;
		this.numPiezas = numPiezas;
		this.fecha = fecha;
	}

	//=============================

	public int getNumLote() {
		return numLote;
	}

	public void setNumLote(int numLote) {
		this.numLote = numLote;
	}

	public int getNumPiezas() {
		return numPiezas;
	}

	public void setNumPiezas(int numPiezas) {
		this.numPiezas = numPiezas;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	//=============================

	public float costoProduccion(Prenda e) {
		float i=0;
		i = e.getCostoProduccion() * this.numPiezas;
		return i;
	}

	public float montoProduccionxlote(Prenda e) {
		float i=0;
		i = costoProduccion(e) * 0.15F;
		return i;
	}

	public float montoRecuperacionxpieza(Prenda e) {
		float i=0;
		i = e.getCostoProduccion() * 0.15F;
		return i;
	}
	
	//============================================

	@Override
	public String toString() {
		return "(Numero de lote: " + numLote + ", Numero de piezas: " + numPiezas + ", Fecha: " + fecha + ")";
	}

	@Override
	public int compareTo(Lote o) {
		return Comparator.comparingInt(Lote::getNumLote)
				.thenComparing(Lote::getFecha)
				.thenComparingInt(Lote::getNumPiezas)
				.compare(this, o);
	}
	
	@Override
	public boolean equals(Object obj) {
		Lote o = (Lote) obj;
		if (numLote == o.getNumLote())
			if (fecha.equals(o.getFecha()))
				if (numPiezas == o.getNumPiezas())
					return true;
		return false;
	}
	
	
}
