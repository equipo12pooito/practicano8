package app.ito.poo;
import clases.ito.poo.Fruta;

public class MyApp {
	
	static void run() {
		Fruta f1 = new Fruta("Manzana", 3.5F, 2, 5);
		System.out.println(f1);
		System.out.println();
		f1.agregarPeriodo("Un mes", 4000);
		System.out.println(f1);
	}
	
	static void run2() {
		Fruta f1 = new Fruta("Manzana", 3.5F, 2, 5);
		System.out.println(f1);
		System.out.println();
		f1.agregarPeriodo("Febrero", 4000);
		f1.agregarPeriodo("Junio y Julio", 7500);
		System.out.println(f1);
		System.out.println();
		System.out.println(f1.getPeriodos());
		System.out.println(f1.devolverPeriodo(3));
		System.out.println(f1.devolverPeriodo(1));
		System.out.println();
		System.out.println(f1.costoPeriodo(f1.devolverPeriodo(3)));
		System.out.println(f1.costoPeriodo(f1.devolverPeriodo(1)));
		System.out.println(f1.gananciaEstimada(f1.devolverPeriodo(1)));
		System.out.println();
		
		Fruta f2 = new Fruta("Manzana", 3.5F, 2, 5);
		Fruta f3 = new Fruta("Durazno", 8F, 5, 7);
		Fruta f4 = new Fruta("Manzana", 8F, 5, 7);
		
		if (f1 == f2)
			System.out.println("Los objetos son iguales");
		else
			System.out.println("Los objetos no son iguales");
		
		System.out.println();
		System.out.println(f1.compareTo(f2));
		System.out.println(f1.equals(f2));
		System.out.println();
		System.out.println(f1.compareTo(f3));
		System.out.println(f1.equals(f3));
		System.out.println();
		System.out.println(f1.compareTo(f4));
		System.out.println(f1.equals(f4));
		
		System.out.println();
		f1.agregarPeriodo("Febrero", 4000);
		System.out.println(f1.devolverPeriodo(0).compareTo(f1.devolverPeriodo(2)));
		System.out.println(f1.devolverPeriodo(0).equals(f1.devolverPeriodo(2)));
		System.out.println();
		System.out.println(f1.devolverPeriodo(0).compareTo(f1.devolverPeriodo(1)));
		System.out.println(f1.devolverPeriodo(0).equals(f1.devolverPeriodo(1)));
	}
	

	public static void main(String[] args) {
		//run();
		run2();
	}

}