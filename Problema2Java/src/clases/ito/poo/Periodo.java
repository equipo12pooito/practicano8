package clases.ito.poo;

import java.util.Comparator;

public class Periodo implements Comparable <Periodo> {

	private String tiempoCosecha;
	private float cantCosechaxtiempo;
	
	public Periodo() {
		super();
	}
	
	public Periodo(String tiempoCosecha, float cantCosechaxtiempo) {
		super();
		this.tiempoCosecha = tiempoCosecha;
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}

	//=============================

	public String getTiempoCosecha() {
		return tiempoCosecha;
	}

	public void setTiempoCosecha(String tiempoCosecha) {
		this.tiempoCosecha = tiempoCosecha;
	}

	public float getCantCosechaxtiempo() {
		return cantCosechaxtiempo;
	}

	public void setCantCosechaxtiempo(float cantCosechaxtiempo) {
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}

	//=============================

	@Override
	public String toString() {
		return "(Tiempo de cosecha: " + tiempoCosecha + ", Cantidad de cosecha: " + cantCosechaxtiempo + ")";
	}

	@Override
	public int compareTo(Periodo o) {
		return Comparator.comparing(Periodo::getTiempoCosecha)
				.thenComparingDouble(Periodo::getCantCosechaxtiempo)
				.compare(this, o);
	}

	@Override
	public boolean equals(Object obj) {
		Periodo o = (Periodo) obj;
		if (tiempoCosecha == o.getTiempoCosecha())
			if (cantCosechaxtiempo == o.getCantCosechaxtiempo())
				return true;
		return false;
	}
	
}